#include <stdio.h>
#include <stdlib.h>
#include "Header.h"

float median(float p_a[], int abc)
{	
	int i, j; 
	float temp;
	for (i = 0; i<abc-1; i++)
    {
		for (j=0; j<abc-1-i; j++)
		{
			if (p_a[j] > p_a[j+1])
			{
				temp = p_a[j+1];
				p_a[j+1] = p_a[j];
				p_a[j] = temp;
			}
		}
    }
	float ab = 0.0;
	int c = abc/2;	
	if (abc%2==0)
	{
		ab = (p_a[c]+p_a[c-1])/2;
	}
	else
	{
		ab = p_a[c];
	}
return ab;		
}
