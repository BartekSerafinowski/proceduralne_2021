#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "Header.h"

struct data
{
float X[50];
float Y[50];
float RHO[50];
float mean[3];
float median[3];
float var[3];
};

void bubblesort(float table[], int size);

int main()
{
FILE *file;

file = fopen("P0001_atr.txt" ,"ra+");

int d = 0;
int e = 1;
float o = 0.0;
float k = 0.0;
float m = 0.0;
float n = 0.0;
struct data data1;
char pl[50] = "";

fgets(pl, 50, file);
printf("%s", pl);

while (fscanf(file, "%f", &n) != EOF) //zczytywanie wartości do tablic w strukturze 
{
	fscanf(file, "%f\t%f\t%f\n",&k, &m, &n);
	data1.X[d] = k;
	data1.Y[d] = m;
	data1.RHO[d] = n;
	printf("%d.\t%f\t%f\t%f\n",e,data1.X[d],data1.Y[d],data1.RHO[d]);
	d++;
	e++;
}

data1.mean[0] = mean(data1.X, d);	//wylicanie i wczytywanie wartości statystycznych do tablic w strukturze 
data1.median[0] = median(data1.X, d);
data1.var[0] = dev(data1.X, d,data1.mean[0]);
data1.mean[1] = mean(data1.Y, d);
data1.median[1] = median(data1.Y, d);
data1.var[1] = dev(data1.Y, d, data1.mean[1]);
data1.mean[2] = mean(data1.RHO, d);
data1.median[2] = median(data1.RHO, d);
data1.var[2] = dev(data1.RHO, d,data1.mean[2]);

printf("Mean\t%f\t%f\t%f\n",data1.mean[0],data1.mean[1],data1.mean[2]);
printf("Median\t%f\t%f\t%f\n",data1.median[0],data1.median[1],data1.median[2]);
printf("SD\t%f\t%f\t%f\n",data1.var[0],data1.var[1],data1.var[2]);


while (fscanf(file, "%f", &data1.mean[0]) == EOF) //wczytywanie do pliku wartości statycznycznych w razie ich braku
{
	fprintf(file, "\nMean:\t%f\t%f\t%f\n", data1.mean[0], data1.mean[1], data1.mean[0]);
	fprintf(file, "Median:\t%f\t%f\t%f\n", data1.median[0], data1.median[1], data1.median[2]);
	fprintf(file, "SD:\t%f\t%f\t%f\n", data1.var[0], data1.var[1], data1.var[2]);
}

fclose(file);

return 0;
}