#ifndef FUNKCJE_H
#define FUNKCJE_H

float srednia(float *tab, int size);
float mediana(float *tab, int size);
float odchylenie(float *tab, int size);
void sortowanie(float *tab, int roz);

#endif