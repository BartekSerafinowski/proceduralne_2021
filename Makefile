CC=gcc
CFLAGS=-Wall
LDLIBS=-lm

main: main.o sred.o medi.o dev.o
	$(CC) $(CFLAGS) -o main main.o sred.o medi.o dev.o $(LDLIBS)
main.o: main.c Header.h
	$(CC) $(CFLAGS) -c main.c 
sred.o: sred.c
	$(CC) $(CFLAGS) -c sred.c 
medi.o: medi.c 
	$(CC) $(CFLAGS) -c medi.c
dev.o: dev.c
	$(CC) $(CFLAGS) -c dev.c