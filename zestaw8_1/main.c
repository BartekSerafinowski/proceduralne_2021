#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "funkcje.h"

void sortowanie(int *tab, int roz)
{
    int u=0;
    int w=0;
    int i=1;

    for(i=1; i<roz; i++)
    {
        u=tab[i];
        w=i-1;

        while(w>=0 && tab[w]>u)
        {
            tab[w+1]=tab[w];
            --w;
        }
        tab[w+1]=u;
    }
    return; 
}

//funkcja do zliczenia liczby linijek w pliku
int licz(FILE *plik1)
{
    plik1 = fopen("P0001_attr.rec", "a+");
    char z = ' ';
    int linie = 0;

    for (z = getc(plik1); z!=EOF; ch=getc(plik1)) //zliczmy znak po znaku
    { 
        if (z == '\n')
            linie++;
    }
    fclose(plik1);
    return linie;
}

int main()
{
    FILE *plik;
    plik = fopen("P0001_attr.rec", "a+");

    int i=0;

    //linijka pomocnicza - nagłówek z pliku
    char lp[50] = "";
    
    int linie = licz(myFile);
    
    //alokacja tablic dynamicznych
    float  *x;
    float  *y;
    float  *rho;
    float  *n; 

    x = (float*)malloc(linie*sizeof(float));
    y = (float*)malloc(linie*sizeof(float));
    rho = (float*)malloc(linie*sizeof(float));
    n = (float*)malloc(linie*sizeof(float)); 

    //wczytywanie danych z pliku do tablic
    //wczytuje linijke "LP X Y RHO"

    fgets(lp, 50, plik); 
    for(i=0; i<linie; i++) 
    {
        fscanf(plik, "%f\t%f \t%f\t%f", &n[i], &x[i], &y[i], &rho[i]);
    }

    for(i=0; i<linie; i++)
    {
        printf("%.2f  %.2f  %.2f \n", x[i], y[i], rho[i]);
    }
    
    sortowanie(x, linie);
    sortowanie(y, linie);
    sortowanie(rho, linie);
    printf("\n\nOdchylenie std.: %f %f %f", odchylenie(x, linie), odchylenie(y, linie), odchylenie(rho, linie));
    printf("\n\nSrednia: %f %f %f", srednia(x, linie), srednia(y, linie), srednia(rho, linie));
    printf("\n\nMediana: %f %f %f", mediana(x, linie), mediana(y, linie), mediana(rho, linie));
    
    free(x);
    free(y);
    free(rho);
    fclose(plik);

    return 0;
}