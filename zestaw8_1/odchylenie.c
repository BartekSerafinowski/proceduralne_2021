# include "funkcje.h"
# include <math.h>

float odchylenie(float *tab1, int roz1) 
{

    float suma=0.0; 
    float srednia=0.0;
    float odch=0.0;
    int i=0;

    for (i=0; i<roz1; i++) 
    {
        suma=suma+tab1[i];
    }
    srednia=suma/roz1;

    for (i=0; i<roz1; i++)
    {
        odch=odch+pow(tab1[i]-srednia, 2);
    }
    odch=sqrt(odch/roz1);

    return odch;
}