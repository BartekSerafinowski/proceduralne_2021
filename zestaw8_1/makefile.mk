CC=gcc
CFLAGS=-Wall
LDLIBS=-lm

main: main.o srednia.o odchylenie.o mediana.o
	$(CC) $(CFLAGS) -o main main.o srednia.o odchylenie.o mediana.o $(LDLIBS)

main.o: main.c funkcje.h 
	$(CC) $(CFLAGS) -c main.c 

srednia.o: srednia.c
	$(CC) $(CFLAGS) -c srednia.c

odchylenie.o: odchylenie.c
	$(CC) $(CFLAGS) -c odchylenie.c

mediana.o: mediana.c
	$(CC) $(CFLAGS) -c mediana.c
